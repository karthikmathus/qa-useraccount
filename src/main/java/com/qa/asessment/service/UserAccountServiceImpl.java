package com.qa.asessment.service;

import com.qa.asessment.dto.UserAccount;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;

@Service
public class UserAccountServiceImpl implements UserAccountService {

    private static int accountNumber = 0;
    private static final SortedSet<UserAccount> userRepository = new TreeSet();

    public boolean storeAccount(UserAccount userAccount) {
        userAccount.setId(getNewAccountNumber());
        return userRepository.add(userAccount);
    }

    public Optional<UserAccount> fetchAccount(int accountNumber) {
        return userRepository.stream().filter(userAccount -> userAccount.getId() == accountNumber)
                .findFirst();

    }

    @Override
    public List<UserAccount> fetchAccounts() {
        return new ArrayList<>(userRepository);
    }

    public boolean deleteAccount(int accountNumber) {
        return userRepository.removeIf(userAccount -> userAccount.getId() == accountNumber);
    }

    private synchronized int getNewAccountNumber() {
        accountNumber = accountNumber + 1;
        return accountNumber;
    }

}
