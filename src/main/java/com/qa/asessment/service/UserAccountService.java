package com.qa.asessment.service;

import com.qa.asessment.dto.UserAccount;

import java.util.List;
import java.util.Optional;


public interface UserAccountService {

     boolean storeAccount(UserAccount userAccount);

     Optional<UserAccount> fetchAccount(int accountNumber);

     List<UserAccount> fetchAccounts();

     boolean deleteAccount(int accountNumber);

}
