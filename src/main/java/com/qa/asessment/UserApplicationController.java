package com.qa.asessment;

import com.qa.asessment.dto.Message;
import com.qa.asessment.dto.UserAccount;
import com.qa.asessment.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class UserApplicationController {


    private UserAccountService userAccountService;

    @Autowired
    public UserApplicationController(UserAccountService userAccountService){
        this.userAccountService = userAccountService;
    }


    @RequestMapping(path = "/account-project/rest/account/json", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> createUserAccount(@RequestBody UserAccount userAccount){
        Message returnMessage;
        if(userAccountService.storeAccount(userAccount)) {
            returnMessage =  new Message("account has been successfully added");
            return new ResponseEntity<>(returnMessage, HttpStatus.OK);
        }else{
            returnMessage = new Message("unable to create an account");
            return new ResponseEntity<>(returnMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path = "/account-project/rest/account/json/{accountId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> removeUserAccount(@PathVariable("accountId") int accountId){
        Message returnMessage;
        if(userAccountService.deleteAccount(accountId)) {
            returnMessage = new Message("account successfully deleted");
            return new ResponseEntity(returnMessage, HttpStatus.OK);
        }else{
            returnMessage = new Message("unable to delete account");
            return new ResponseEntity(returnMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path = "/account-project/rest/account/json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserAccount>> fetchAllAccount(){
        return new ResponseEntity(userAccountService.fetchAccounts(), HttpStatus.OK);
    }

    @RequestMapping(path = "/account-project/rest/account/json/{accountId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserAccount> fetchUserAccount(@PathVariable("accountId") int accountId){
        Optional<UserAccount> userAccount = userAccountService.fetchAccount(accountId);
        if(userAccount.isPresent()){
            return new ResponseEntity(userAccount.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
