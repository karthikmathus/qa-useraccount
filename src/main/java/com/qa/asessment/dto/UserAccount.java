package com.qa.asessment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UserAccount implements Comparable {

    private int id;
    private String firstName;
    private String secondName;
    private String accountNumber;

    public UserAccount(@JsonProperty("firstName") String firstName, @JsonProperty("secondName") String secondName,
                       @JsonProperty("accountNumber") String accountNumber) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.accountNumber = accountNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }


    public String getSecondName() {
        return secondName;
    }


    public String getAccountNumber() {
        return accountNumber;
    }


    @Override
    public int compareTo(Object o) {
        return ((UserAccount) o).getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(secondName, that.secondName) &&
                Objects.equals(accountNumber, that.accountNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(firstName, secondName, accountNumber);
    }
}
