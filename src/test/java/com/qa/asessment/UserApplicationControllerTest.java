package com.qa.asessment;

import com.qa.asessment.dto.Message;
import com.qa.asessment.dto.UserAccount;
import com.qa.asessment.service.UserAccountService;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class UserApplicationControllerTest {

    UserAccountService mockAccountService = Mockito.mock(UserAccountService.class);

    private final UserApplicationController classToTest = new UserApplicationController(mockAccountService);

    @Test
    public void createUserAccount() {
        UserAccount userAccount = new UserAccount("Karthik", "Kannan", "AC101");

        when(mockAccountService.storeAccount(any())).thenReturn(true);
        ResponseEntity<Message> createResponse = classToTest.createUserAccount(userAccount);

        assertThat("Return message is wrong", createResponse.getBody().getMessage(), is("account has been successfully added"));
        assertThat("Status code returned is not correct", createResponse.getStatusCode(), is(HttpStatus.OK));

        when(mockAccountService.storeAccount(any())).thenReturn(false);
        ResponseEntity<Message> failResponse = classToTest.createUserAccount(userAccount);

        assertThat("Return message is wrong", failResponse.getBody().getMessage(), is("unable to create an account"));
        assertThat("Status code returned is not correct", failResponse.getStatusCode(), is(HttpStatus.BAD_REQUEST));

    }

    @Test
    public void removeUserAccount() {
        when(mockAccountService.deleteAccount(anyInt())).thenReturn(true);
        ResponseEntity<Message> removeResponse = classToTest.removeUserAccount(1);

        assertThat("Return message is wrong", removeResponse.getBody().getMessage(), is("account successfully deleted"));
        assertThat("Status code returned is not correct", removeResponse.getStatusCode(), is(HttpStatus.OK));


        when(mockAccountService.deleteAccount(anyInt())).thenReturn(false);
        ResponseEntity<Message> errorResponse = classToTest.removeUserAccount(1);

        assertThat("Return message is wrong", errorResponse.getBody().getMessage(), is("unable to delete account"));
        assertThat("Status code returned is not correct", errorResponse.getStatusCode(), is(HttpStatus.BAD_REQUEST));


    }

    @Test
    public void fetchAllAccount() {
        when(mockAccountService.fetchAccounts()).thenReturn(Collections.emptyList());
        ResponseEntity<List<UserAccount>> listAllResponse = classToTest.fetchAllAccount();
        assertThat("Http response code should be 200", listAllResponse.getStatusCode(), is(HttpStatus.OK));

        UserAccount userAccount = new UserAccount("Karthik", "Kannan", "AC101");

        when(mockAccountService.fetchAccounts()).thenReturn(Collections.singletonList(userAccount));
        listAllResponse = classToTest.fetchAllAccount();

        assertThat("Http response code should be 200", listAllResponse.getStatusCode(), is(HttpStatus.OK));
        assertThat("Response should have one user account", listAllResponse.getBody().size(), is(1));

    }

    @Test
    public void fetchUserAccount() {
        UserAccount userAccount = new UserAccount("Karthik", "Kannan", "AC101");

        when(mockAccountService.fetchAccount(anyInt())).thenReturn(Optional.of(userAccount));
        ResponseEntity<UserAccount> fetchResponse = classToTest.fetchUserAccount(1);

        assertThat("Response should have one user account", fetchResponse.getBody(), is(userAccount));
        assertThat("Response should have one user account", fetchResponse.getStatusCode(), is(HttpStatus.OK));

        when(mockAccountService.fetchAccount(anyInt())).thenReturn(Optional.empty());
        fetchResponse = classToTest.fetchUserAccount(2);

        assertThat("Response should have one user account", fetchResponse.getStatusCode(), is(HttpStatus.NOT_FOUND));

    }
}