package com.qa.asessment;

import com.qa.asessment.dto.UserAccount;
import org.apache.http.entity.ContentType;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.with;
import static org.hamcrest.Matchers.equalTo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserApplicationIT {


    @Test
    public void createAccountTest() {
        UserAccount userAccount = new UserAccount("Karthik", "Kannan", "AC101");
        with().contentType(ContentType.APPLICATION_JSON.toString()).body(userAccount).
                put("/account-project/rest/account/json").then().assertThat()
                .body("message", equalTo("account has been successfully added"));

    }

    @Test
    public void fetchAccountTest() {
        given().when().pathParam("accountId", 1).
                get("/account-project/rest/account/json/{accountId}").then()
                .body("id", equalTo(1))
                .body("firstName", equalTo("Karthik"))
                .body("secondName", equalTo("Kannan"))
                .body("accountNumber", equalTo("AC101"))
                .statusCode(200);

    }

    @Test
    public void fetchAllAccountsTest() {
        UserAccount userAccount = new UserAccount("Meghan", "Markle", "AC102");
        with().contentType(ContentType.APPLICATION_JSON.toString()).body(userAccount).
                put("/account-project/rest/account/json").then().assertThat()
                .body("message", equalTo("account has been successfully added"));

        given().when().
                get("/account-project/rest/account/json").then()
                .body("id[1]", equalTo(2))
                .body("firstName[1]", equalTo("Meghan"))
                .body("secondName[1]", equalTo("Markle"))
                .body("accountNumber[1]", equalTo("AC102"))
                .body("id[0]", equalTo(1))
                .body("firstName[0]", equalTo("Karthik"))
                .body("secondName[0]", equalTo("Kannan"))
                .body("accountNumber[0]", equalTo("AC101"))
                .statusCode(200);

    }

    @Test
    public void removeAccountTest() {
        given().when().pathParam("accountId", 1).
                delete("/account-project/rest/account/json/{accountId}").then()
                .statusCode(200);

        given().when().pathParam("accountId", 1).
                get("/account-project/rest/account/json/{accountId}").then()
                .statusCode(404);

    }


}
