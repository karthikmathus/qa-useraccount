package com.qa.asessment.service;

import com.qa.asessment.dto.UserAccount;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserAccountServiceImplTest {

    private UserAccountService classToTest = new UserAccountServiceImpl();

    private final static Predicate<UserAccount> accountPredicate(UserAccount userAccount) {
        return x -> x.getFirstName().equalsIgnoreCase(userAccount.getFirstName()) &&
                x.getSecondName().equalsIgnoreCase(userAccount.getSecondName()) &&
                x.getAccountNumber().equalsIgnoreCase(userAccount.getAccountNumber());
    }


    @Test
    public void createAccountTest() {
        UserAccount userAccount = new UserAccount("Karthik", "Kannan", "AC101");

        classToTest.storeAccount(userAccount);

        Optional<UserAccount> userAccountReturned = classToTest.fetchAccount(1);

        assertTrue("User Account should have been stored", userAccountReturned.isPresent());
        assertThat("User Account should have got Id set", userAccountReturned.get().getId(), is(1));
        assertThat("User Account firstName is not stored", userAccountReturned.get().getFirstName(), is("Karthik"));
        assertThat("User Account secondName is not stored", userAccountReturned.get().getSecondName(), is("Kannan"));
        assertThat("User Account account number is not stored", userAccountReturned.get().getAccountNumber(), is("AC101"));

    }

    @Test
    public void fetchAccountTest() {

        UserAccount userAccount = new UserAccount("Meghan", "Markel", "AC102");

        classToTest.storeAccount(userAccount);

        Optional<UserAccount> userAccountReturned = classToTest.fetchAccount(2);

        assertTrue("User Account should have been stored", userAccountReturned.isPresent());
        assertThat("User Account should have got Id set", userAccountReturned.get().getId(), is(2));
        assertThat("User Account firstName is not stored", userAccountReturned.get().getFirstName(), is("Meghan"));
        assertThat("User Account secondName is not stored", userAccountReturned.get().getSecondName(), is("Markel"));
        assertThat("User Account account number is not stored", userAccountReturned.get().getAccountNumber(), is("AC102"));

    }

    @Test
    public void fetchAccountsTest() {

        UserAccount userAccount = new UserAccount("Meghan", "Markel", "AC102");
        UserAccount userAccount1 = new UserAccount("Meghan1", "Markel1", "AC103");
        UserAccount userAccount2 = new UserAccount("Meghan2", "Markel2", "AC104");
        UserAccount userAccount3 = new UserAccount("Meghan3", "Markel3", "AC105");

        classToTest.storeAccount(userAccount);
        classToTest.storeAccount(userAccount1);
        classToTest.storeAccount(userAccount2);
        classToTest.storeAccount(userAccount3);

        List<UserAccount> allAccounts = classToTest.fetchAccounts();

        assertTrue(allAccounts.stream().filter(accountPredicate(userAccount)).findAny().isPresent());
        assertTrue(allAccounts.stream().filter(accountPredicate(userAccount1)).findAny().isPresent());
        assertTrue(allAccounts.stream().filter(accountPredicate(userAccount2)).findAny().isPresent());
        assertTrue(allAccounts.stream().filter(accountPredicate(userAccount3)).findAny().isPresent());


    }

    @Test
    public void deleteAccountTest() {
        UserAccount userAccount = new UserAccount("Meghan", "Markel", "AC102");

        classToTest.storeAccount(userAccount);

        assertTrue(classToTest.fetchAccount(1).isPresent());
        classToTest.deleteAccount(1);
        assertFalse(classToTest.fetchAccount(1).isPresent());

    }

}